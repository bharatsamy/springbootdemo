package com.svs.springbootdemo.controller;

import com.svs.springbootdemo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Autowired
    TestService testService;

    @GetMapping("/")
    public String home(){
        return "welcome to spring boot app";
    }

    @GetMapping("/hello")
    public String hello(){
        return "Hello world !";
    }

    @GetMapping("/raju")
    public String raju(){
        return "Hello raju !";
    }

    @GetMapping("/bharat")
    public String bharat(){
        return "Hello bharat !";
    }

    @GetMapping("/kadu")
    public String kadu(){
        return "Hello kadu kamanati !";
    }



}
