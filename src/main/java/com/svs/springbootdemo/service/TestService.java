package com.svs.springbootdemo.service;

import org.springframework.stereotype.Component;

@Component
public class TestService {

    public String sendMessage(String msg){
        return msg;
    }
}
