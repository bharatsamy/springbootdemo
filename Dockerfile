FROM openjdk:8-jre-alpine
COPY target/springbootdemo-*.jar /usr/src/myapp/springbootdemo.jar
WORKDIR /usr/src/myapp

ENV MINMEM=500M
ENV MAXMEM=600M

EXPOSE 8090
EXPOSE 9010

CMD java -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9010 -Dcom.sum.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Xmx${MAXMEM} -Xms${MINMEM} -jar springbootdemo.jar
